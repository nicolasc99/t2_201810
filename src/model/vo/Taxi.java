package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{


	
	String taxi_id;
	
	String company;
	
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	


	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return (o.getTaxiId().equals(taxi_id))?0: o.getTaxiId().compareTo(taxi_id);
	}	
}

package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	
	String company;
	String trip_id;
	String taxi_id;
	int seconds;
	double milles;
	double total;
	public int dropoff_community_area;

	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	

	
	public int getDropoff_community_area() {
		return dropoff_community_area;
	}
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return milles;
	}
	
	public String getCompany() {
		return company;
	}



	public void setCompany(String company) {
		this.company = company;
	}



	public String getTrip_id() {
		return trip_id;
	}



	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}


	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}



	public int getSeconds() {
		return seconds;
	}



	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}



	public double getMilles() {
		return milles;
	}



	public void setMilles(double milles) {
		this.milles = milles;
	}



	public double getTotal() {
		return total;
	}



	public void setTotal(double total) {
		this.total = total;
	}



	public void setDropoff_community_area(int dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}



	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return total;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}

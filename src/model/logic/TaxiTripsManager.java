package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.DoubleNode;
import model.data_structures.LinkedList;
import model.data_structures.Lista;

import com.google.gson.*;

public class TaxiTripsManager implements ITaxiTripsManager {

	
	private LinkedList<Service> listaEncadenada; 
	private LinkedList<Taxi> listaTaxi; 
	boolean bool = false;


	
	
	public void loadServices (String serviceFile) {
		long tiempo = System.currentTimeMillis();

		// TODO Auto-generated method stub

		listaEncadenada = new Lista<>();
		listaTaxi = new Lista<>();

	
		if (!bool)
		{
		
		try
		{
			Gson gson = new GsonBuilder().create();
			Service[] item= gson.fromJson(new FileReader(serviceFile), Service[].class);
			Taxi[] taxi= gson.fromJson(new FileReader(serviceFile), Taxi[].class);

			for(int i=0; i<item.length;i++)
			{
				
				listaEncadenada.aņadirPrimerElemento(item[i]);
				
			}
			

			for(int i=0; i<taxi.length;i++)
			{
				
				
				if(!listaTaxi.contains(taxi[i]))
					
				{
					listaTaxi.aņadirPrimerElemento(taxi[i]);
					
				}
				
				
				}
			
			
			bool=true;
			
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Hubo un error al cargar el archivo");
		}
		
			
		System.out.println(listaEncadenada.size()+" Services");
		System.out.println(listaTaxi.size() + " Taxis");
		System.out.println((System.currentTimeMillis()-tiempo)/1000.0 + " Seconds	");
		System.out.println("Inside loadServices with " + serviceFile);
		}
		
		else
			
		{
			System.err.println("El archivo solamente puede ser cargado una vez en el programa");
			
			
		}

	}
	

	

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub

	
		LinkedList<Taxi> taxis = new Lista<>();
		System.out.println("Inside getTaxisOfCompany with " + company);

		
		for(Taxi taxi: listaTaxi  )
		{
			
			if(taxi!=null&&taxi.getCompany()!=null && taxi.getCompany().equalsIgnoreCase(company))
			{
			
			taxis.aņadirPrimerElemento(taxi);
			
			}
		}
		
		
	System.out.println(taxis.size());

		return taxis;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		
		LinkedList<Service> lista_dropoff = new Lista<>();
		System.out.println("Inside getTaxisOfCompany with the area" +" " +communityArea);

		for(Service item: listaEncadenada  )
		{
			
			if(item!=null&& item.getDropoff_community_area()==communityArea)
			{
			
				lista_dropoff.aņadirPrimerElemento(item);
			
			}
		}
		
		
		System.out.println(lista_dropoff.size());
		return lista_dropoff;
	}


}

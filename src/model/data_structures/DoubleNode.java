package model.data_structures;


public class DoubleNode<T extends Comparable<T>> {

	private T element;
	private DoubleNode<T> prev;
	private DoubleNode<T> next;

	public DoubleNode(T e, DoubleNode<T> p, DoubleNode<T> n)
	{
		element = e;
		prev = p;
		next = n;
	}

	public T getElement()
	{
		return element;
	}

	public void setElement(T e)
	{
		element = e;
	}

	public DoubleNode<T> getPrev()
	{
		return prev;
	}

	public DoubleNode<T> getNext()
	{
		return next;
	}

	public void setPrev(DoubleNode<T> p)
	{
		prev = p;
	}

	public void setNext(DoubleNode<T> n)
	{
		next = n;
	}

	
	

}

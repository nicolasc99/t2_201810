package model.data_structures;

public interface LinkedList <T extends Comparable<T>> extends Iterable<T>
{
	public int size();
	
	public boolean isEmpty();
	
	public void añadirPrimerElemento(T e);
	
	public void añadirUltimoElemento(T e);
	
	public T borrarPrimero();
	
	public DoubleNode<T> primero();
	
	public DoubleNode<T> ultimo();
	
	public T buscarElemento(int i);
	
	public T getElement(int e);
	
	public DoubleNode<T> darPrimero();
	
	public boolean contains(T e);
}

package model.data_structures;

import java.util.Iterator;


public class Lista <T extends Comparable<T>> implements LinkedList<T>, Iterable<T>
{
	
	
	private DoubleNode<T> primero;
	private DoubleNode<T> siguiente;
	private int size=0;

	public Lista()
	
	{
		primero = new DoubleNode<T>(null,null,null);
		siguiente = new DoubleNode<T>(null,primero,null);
		primero.setNext(siguiente);
		
	}
	
	
	public int size()
	{
		return size;
	}
	
	
	public boolean isEmpty()
	{
		return size==0;
		
	}
	
	public void añadirPrimerElemento(T e)
	{
		if(size==0)
		{
			
			primero.setElement(e);
			siguiente.setElement(e);
			size++;
			
		}		
		
		else
		{
			DoubleNode<T> nuevo = new DoubleNode<>(e,null,primero);
			primero.setPrev(nuevo);
			primero = nuevo;
			size++;
			
			
		}
		
		
	}
	
	
	public void añadirUltimoElemento(T e)
	{
		if(size==0)
		{
			
			primero.setElement(e);
			siguiente.setElement(e);
			size++;
		}		
		
		else 
		{
			DoubleNode<T> nuevo = new DoubleNode<>(e,siguiente,null);
			siguiente.setNext(nuevo);
			siguiente = nuevo;
			size++;
			
		}
		
	}
	
	
	
	public T borrarPrimero()
	{
		
		DoubleNode<T> temp = primero;
		
		
		if(primero.getElement()==null)
		{
			return null;
			
		}
		
		else if(primero.getNext().getElement()==null)
		{
			primero.setElement(null);
			size--;
			return null;
			
		}
		
		else 
		{
			primero.setElement(null);
			primero = temp.getNext();
			size--;
			return (T) primero.getElement();
			
		}
		
	
	}
	
	
	public T borrarUltimo()
	{
		
		DoubleNode<T> temp = siguiente;
		
		if(primero.getElement()==null)
		{
			return null;
		}
		
		else if (primero.getNext().getElement()==null)
		{
			
			primero.setElement(null);
			size--;
			return null;
			
		}
		
		else
		{
			siguiente.setElement(null);
			siguiente = temp.getNext();
			size--;
			return(T) siguiente.getElement();
			
		}

	}
	
	
	public DoubleNode<T> primero()
	{
		if(isEmpty())
			return null;
		
		return primero.getNext();
		
	}
	

	
	
	public DoubleNode<T> ultimo()
	{
		if(isEmpty())
			return null;
		
		return primero.getPrev();
		
	}
	
	
	
	
	public T buscarElemento(int i)
	{
		T encontrado=null;
		int contador= 0;
		
		DoubleNode<T> actual= darPrimero();
		
		while(encontrado==null && actual!=null)
		{
			
			if(contador ==i)
			{
				encontrado= actual.getElement();
				
			}
			contador++;
			actual= actual.getNext();
			
		}
		
		return encontrado;
	}
	
	
	
	public DoubleNode<T> darPrimero()
	{
		return primero;
	}
	
	
	
	
	
	public void fijarPrimero(DoubleNode<T> primero)
	
	{
		this.primero = primero;
		
	}
	
	
	public T getElement(int i)
	{
		DoubleNode<T> actual = primero;
		int j = 0;
		while(actual != null && j != i)
		{
			actual = actual.getNext();
			j++;
		}
		return (T) actual;
	}
	
	
	public boolean contains(T nodo)
	{		
		for(T nodito: this)
			
		{
		
			if(nodito!=null &&nodito.compareTo(nodo)==0)
			{
				return true;
			}
			
			
		}
		
		return false;		
	}

	
	
	@Override
	public Iterator<T> iterator() 
	{
		return new ListaIterable();
	}
	private class ListaIterable implements Iterator<T>
	{

		private DoubleNode<T> actual = darPrimero();

		@Override
		public boolean hasNext() 
		{
			return actual!=null;
		}

		@Override
		public T next() 
		{
			T ret = actual.getElement();
			actual = actual.getNext();
			return ret;
		}

		@Override
		public void remove() 
		{

		}
	}

	
	
	
	
	

	
	

}

package model.estructures.test;

import junit.framework.TestCase;
import model.data_structures.DoubleNode;
import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class LinkedListTest extends TestCase {



private Lista lista;

private int numeroElementos;
private int arbitrario;

/**
 * Construye una nueva lista vacia de Integers
 */
public void setupEscenario1( )
{
	lista = new Lista( );
	numeroElementos = 1000;
	
}

/**
 * Construye una nueva lista con 200 elementos inicialados con el valor de la posicion por 2
 */
public void setupEscenario2( )
{
	numeroElementos = 500;
	lista = new Lista( );
	arbitrario = 5;
	
	
	
	for( int cont = 0; cont < numeroElementos; cont++ )
	{
		lista.añadirPrimerElemento(( new Integer( cont * arbitrario ) ));
	}
	
}

/**
 * Prueba que los elementos se est�n ingresando correctamente a partir de la lista
 */
public void testaddFirst( )
{
	setupEscenario1( );

	for( int cont = 0; cont < numeroElementos; cont++ )
	{
		lista.añadirPrimerElemento( new Integer( 5 * cont ) );
		
	}

	// Verificar que la lista tenga la longitud correcta
	assertEquals( "No se adicionaron todos los elementos", numeroElementos, lista.size( ) );


}

/**
 * Prueba que los elementos se est�n ingresando correctamente a partir de la lista
 */
public void testaddLast( )
{
	setupEscenario1( );

	for( int cont = 0; cont < numeroElementos; cont++ )
	{
		lista.añadirPrimerElemento( new Integer( 5 * cont ) );
		
	}

	// Verificar que la lista tenga la longitud correcta
	assertEquals( "No se adicionaron todos los elementos", numeroElementos, lista.size( ) );


}




/**
 * Prueba que se retorne correctamente el primer elemento de la lista
 */
public void testDarPrimero( )
{
	setupEscenario2( );
	
	int tamaño = (lista.size()*arbitrario)-arbitrario;

	DoubleNode nodo = lista.darPrimero();

	Integer elemento = ( Integer )nodo.getElement();

	// Verificar que el primero elemento sea el correcto
	assertEquals( "El primer elemento no es correcto", tamaño, elemento.intValue( ) );

}

}

package controller;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	public static void loadServices( ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
		manager.loadServices( serviceFile );
	}
	
//	public static void loadLargeServices( ) {
//		// To define the dataset file's name 
//		String serviceFile = "./data/taxi-medium.json";
//		
//		manager.loadServices( serviceFile );
//	}
	
		
	public static LinkedList<Taxi> getTaxisOfCompany (String company) {
		return manager.getTaxisOfCompany(company);
	}
	
	public static LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		return manager.getTaxiServicesToCommunityArea( communityArea );
	}
	
	
	
	
}
